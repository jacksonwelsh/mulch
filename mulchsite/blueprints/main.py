from flask import render_template, Blueprint, session, request, g
from ..usertools import get_name

main = Blueprint('main', __name__)


@main.route('/')
def home():
    if 'ref' in request.args:
        session['ref'] = request.args['ref']
    elif 'ref' not in session:
        session['ref'] = None
    name = get_name(session['ref']) if session['ref'] else None
    return render_template('home.html')