from flask_login import login_user, logout_user, login_required
from flask import render_template, redirect, jsonify, Blueprint, request, flash, url_for
from werkzeug.security import check_password_hash
from ..models import *
from .. import app

auth = Blueprint('auth', __name__)


@auth.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    # otherwise, handle login attempts
    username = request.form.get('username')
    password = request.form.get('password')
    user = User.query.filter_by(username=username).first()
    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.')
        return render_template('login.html')
    login_user(user, remember=True)
    return redirect(url_for('main.home'))


@auth.route('/logout')
def logout():
    logout_user()
    flash('Logged out successfully.')
    return redirect(url_for('main.home'))
