from flask import Flask, session, redirect, request, url_for, render_template, Response, g, jsonify
import flask
import psycopg2
import stripe
import phonenumbers
from functools import wraps
import json
from pprint import pprint
from usertools import *
from time import time

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

sentry_sdk.init(
    dsn="https://8a7b9412b4684d84985c156bdccdaf81@sentry.io/1440786",
    integrations=[FlaskIntegration()]
)

app = Flask(__name__)
app.config['SECRET_KEY'] = get_secret('flask', 'key')
stripe.api_key = get_secret('stripe', 'key')

con = psycopg2.connect(host="localhost",
                       database="band",
                       user="ubuntu",
                       password=get_secret('sql', 'password'),
                       port=5432)

auth_vars = ['is_admin', 'ref', 'authenticated_as', 'full_name']


def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    return validate_login(username, password, g.site)


def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
        'Could not verify your access level for that URL.\n'
        'You have to login with proper credentials', 403,
        {'WWW-Authenticate': 'Basic realm="Login Required"'})


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if 'authenticated_as' not in session:
            return redirect(url_for('login'))
        return f(*args, **kwargs)

    return decorated


def requires_admin(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if 'is_admin' not in session:
            return render_template('403.html'), 403
        return f(*args, **kwargs)

    return decorated


@app.before_request
def start():
    if request.host == 'dragonband.j1442.co':
        return
    with con:
        cur = con.cursor()
        cur.execute(
            'select slug, fullname, license, nav_img, header_img, tagline, color, nav_text, header_text from mulch.sites where domain = %s',
            (request.host,))
        data = cur.fetchone()
        g.site, g.name, g.license, g.nav_img, g.head_img, g.tagline, g.color, g.nav_text, g.head_text = data


@app.route('/')
def home():
    print('!>>>')
    [session.pop(k) for k in list(session.keys()) if k not in auth_vars]
    if 'ref' in request.args:
        session['ref'] = request.args['ref']
    elif 'ref' not in session:
        session['ref'] = None
    name = get_name(session['ref'], g.site) if session['ref'] else None
    return render_template('buy.html', name=name)


@app.route('/buy')
def buy():
    if 'errortype' in session:
        error = session['errortype']
    else:
        error = None
    if 'ref' in request.args:
        referrer = session['ref'] = request.args['ref']
    elif 'ref' in session:
        referrer = session['ref']
    else:
        referrer = None
    return render_template('buy.html', error=error, referrer=referrer)


@app.route('/charge', methods=['GET', 'POST'])
def charge():
    name = session['name'] = request.form['name']
    email = session['email'] = request.form['email']
    phone = session['phone'] = request.form['phone']
    phone = phonenumbers.format_number(
        phonenumbers.parse(phone, 'US'),
        phonenumbers.PhoneNumberFormat.E164)[2:]

    address = session['address'] = request.form['address']
    city = session['city'] = request.form['city']
    zipcode = session['zip'] = request.form['zip']
    brown = int(request.form['brown'])
    black = int(request.form['black'])
    instructions = session['instructions'] = request.form['instructions']
    ref = request.form['ref'] if 'ref' in request.form \
        else request.form['refHidden'] if 'refHidden' in request.form \
        else None
    if black + brown < 5:
        session['errortype'] = 'You must order at least five bags of mulch.'
        return redirect(url_for('buy'))
    if black == 0 and brown != 0:
        purchase = [
            {
                'amount': 500,
                'quantity': brown,
                'name': 'Brown Mulch',
                'currency': 'usd',
                'description': 'Brown hardwood mulch',
                'images': ['https://mobileimages.lowes.com/product/converted/742020/742020495014.jpg']
            }]
    elif brown == 0 and black != 0:
        purchase = [
            {
                'amount': 500,
                'quantity': black,
                'name': 'Black Mulch',
                'currency': 'usd',
                'description': 'Black mulch',
                'images': ['https://mobileimages.lowes.com/product/converted/742020/742020495076.jpg']
            }
        ]
    elif brown != 0 and black != 0:
        purchase = [
            {
                'amount': 500,
                'quantity': brown,
                'name': 'Brown Mulch',
                'currency': 'usd',
                'description': 'Brown hardwood mulch',
                'images': ['https://mobileimages.lowes.com/product/converted/742020/742020495014.jpg']
            },
            {
                'amount': 500,
                'quantity': black,
                'name': 'Black Mulch',
                'currency': 'usd',
                'description': 'Black mulch',
                'images': ['https://mobileimages.lowes.com/product/converted/742020/742020495076.jpg']
            }
        ]
    print(purchase)
    with con:
        cur = con.cursor()
        cur.execute('select * from mulch.users where email = %s', (email.lower(),))
        cust = cur.fetchone()
        if not cust:
            customer = stripe.Customer.create(
                email=email,
                shipping={
                    'address':
                        {'line1': address,
                         'city': city,
                         'postal_code': zipcode,
                         'state': 'TX'},
                    'name': name,
                    'phone': phone}
            )
            cur.execute('insert into mulch.users values(%s, %s, %s, %s, %s)',
                        (customer['id'], name, phone, email.lower(), g.site,))
        else:
            customer = stripe.Customer.retrieve(cust[0])

        chsession = stripe.checkout.Session.create(
            customer=customer['id'],
            payment_intent_data={
                'description': f'{brown} bags brown mulch and {black} bags black mulch @ $5.00/ea.',
                'metadata': {
                    'referrer': ref.lower(),
                    'instructions': instructions
                }
            },
            success_url='https://mulch.j1442.co/thanks',
            cancel_url='https://mulch.j1442.co/',
            payment_method_types=['card'],
            billing_address_collection='auto',
            line_items=purchase
        )
    session['customer'] = customer['id']
    return render_template('charge.html', sessionid=chsession['id'])


@app.route('/thanks')
def thanks():
    if 'customer' not in session:
        return redirect(url_for('home'))
    customer = stripe.Customer.retrieve(session['customer'])
    name = customer['shipping']['name']
    address = customer['shipping']['address']['line1']
    if customer['shipping']['address']['line2']:
        address += ', ' + str(customer['shipping']['address']['line2'])
    address = address.strip()
    city = customer['shipping']['address']['city']
    zipcode = customer['shipping']['address']['postal_code']
    phone = customer['shipping']['phone']
    phone = phonenumbers.format_number(phonenumbers.parse(phone, 'US'), phonenumbers.PhoneNumberFormat.NATIONAL)
    email = customer['email']
    return render_template('thanks.html', name=name, address=address, city=city, zip=zipcode, phone=phone, email=email)


@app.route('/lookup', methods=['GET', 'POST'])
def lookup():
    if request.method == 'GET' and ('email' and 'phone') not in request.args:
        return render_template('lookup.html')
    receipts = {}
    names = {}
    if 'email' and 'phone' in request.args:
        phone = request.args['phone']
        email = request.args['email']
    else:
        phone = request.form['phone']
        email = request.form['email']
    ph = phonenumbers.format_number(
        phonenumbers.parse(phone, 'US'),
        phonenumbers.PhoneNumberFormat.E164)[2:]
    with con:
        cur = con.cursor()
        cur.execute('select * from mulch.orders where email = %s and phone = %s', (email, ph,))
        orders = cur.fetchall()
    for o in orders:
        names[o[0]] = get_name(o[8], g.site)
        if o[0][0:2] == 'pi':
            pi = stripe.PaymentIntent.retrieve(o[0])
            receipts[o[0]] = pi['charges']['data'][0]['receipt_url']
    phonef = phonenumbers.format_number(
        phonenumbers.parse(phone, 'US'),
        phonenumbers.PhoneNumberFormat.NATIONAL)
    if not orders:
        return render_template('lookup.html', email=email, phone=phonef)
    return render_template('results.html', email=email, phone=phonef, orders=orders, receipts=receipts, names=names)


@app.route('/manual-entry', methods=['GET', 'POST'])
@requires_auth
def manual_entry():
    if 'success' in session:
        success = True
        orderid = session['orderid']
    else:
        success = None
        orderid = None
    [session.pop(k) for k in list(session.keys()) if k not in auth_vars]
    if request.method == 'GET':
        return render_template('manual.html', success=success, orderid=orderid)
    name = session['name'] = request.form['name']
    email = session['email'] = request.form['email']
    phone = session['phone'] = request.form['phone']
    phone = phonenumbers.format_number(
        phonenumbers.parse(phone, 'US'),
        phonenumbers.PhoneNumberFormat.E164)[2:]
    address = session['address'] = request.form['address']
    city = session['city'] = request.form['city']
    zipcode = session['zip'] = request.form['zip']
    brown = int(request.form['brown'])
    black = int(request.form['black'])
    orderid = '_'.join(('man', str(hex(int(time()))[2:])))
    instructions = session['instructions'] = request.form['instructions']
    ref = request.form['ref'] if 'ref' in request.form else request.form['refHidden']
    if black + brown < 5:
        session['errortype'] = 'You must order at least five bags of mulch.'
        return render_template('manual.html')
    with con:
        cur = con.cursor()
        cur.execute('insert into mulch.orders values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)', (
            orderid, name, email, address, zipcode, phone, brown, black, ref, instructions, False, g.site,))

        cur.execute('update mulch.logins set orders = orders + 1, bags = bags + %s where username = %s and site = %s', (
            brown + black, ref, g.site,))

    if 'errortype' in session:
        del session['errortype']
    session['orderid'] = orderid
    session['success'] = True
    return redirect(url_for('manual_entry'))


@app.route('/login')
def login():
    return render_template('login.html')


@app.route('/logout', subdomain='mulch')
@requires_auth
def logout():
    for i in auth_vars:
        if i in session:
            del session[i]
    return redirect(url_for('home'))


@app.route('/auth', methods=['GET', 'POST'])
def authenticate():
    if request.method == 'GET':
        return redirect(url_for('login'))
    user = request.form['username'].lower()
    pin = request.form['pin']
    loginStatus = validate_login(user, pin, g.site)
    print(loginStatus)
    if loginStatus is True or loginStatus is False:
        session['authenticated_as'] = session['ref'] = user.lower()
        session['full_name'] = get_name(user, g.site)
        print(session['full_name'])
        if loginStatus is True:
            print('you\'re an admin, harry!')
            session['is_admin'] = 'admin'
            print(session)
        return redirect(url_for('home'))
    return render_template('login.html', error='Invalid username or PIN.')


@app.route('/edit', methods=['GET', 'POST'])
@requires_auth
def edit():
    if request.method == 'GET':
        return render_template('edit.html')
    set_password(session['authenticated_as'], request.form['pin'], g.site)
    return render_template('edit.html', update=request.form['pin'])


@app.route('/sales')
@requires_auth
def sales():
    bags = 0
    phones = {}
    with con:
        cur = con.cursor()
        cur.execute('select * from mulch.orders where seller = %s and site = %s order by payment ASC, name ASC',
                    (session['authenticated_as'], g.site,))
        orders = cur.fetchall()
        cur.execute('select * from mulch.logins where username = %s and site = %s',
                    (session['authenticated_as'], g.site,))
        userinfo = cur.fetchone()
        if not userinfo:
            userinfo = [0, 0, 0, 0, 0, 0]
    for o in orders:
        bags += (o[6] + o[7])
        phones[o[0]] = format_phone(o[5])
    if not orders:
        return render_template('sales.html', err='There are no orders tied to your account.')
    return render_template('sales.html', count=userinfo[3], orders=orders, bags=userinfo[4], verified=userinfo[5],
                           phones=phones)


@app.route('/admin', methods=['GET', 'POST'])
@requires_admin
def admin():
    if request.method == 'GET':
        return render_template('admin.html')
    if request.args['form'] == 'update':
        if set_password(request.form['usernamepin'], request.form['pinpin'], g.site):
            return render_template('admin.html', user=request.form['usernamepin'], pin=request.form['pinpin'])
        return render_template('admin.html', name=request.form['usernamepin'],
                               error='doesn\'t exist in the database.')
    if request.args['form'] == 'create':
        if create_user(request.form['namecreate'], request.form['usernamecreate'], request.form['pincreate'], g.site):
            return render_template('admin.html', created='y',
                                   name=request.form['usernamecreate'], pin=request.form['pincreate'])
        return render_template('admin.html', error='already exists as a user.', name=request.form['usernamecreate'])
    if request.args['form'] == 'delete':
        delete_user(request.form['usernamedel'], g.site)
        return render_template('admin.html', user=request.form['usernamedel'], delete='y')
    if request.args['form'] == 'view':
        return redirect(f'/sales/{request.form["usernameview"]}')


@app.route('/sales/<user>', methods=['GET', 'POST'])
@requires_admin
def admin_sales(user):
    user = user.lower()
    bags = 0
    fullname = get_name(user, g.site)
    phones = {}
    alert = None
    if request.method == 'POST':
        with con:
            cur = con.cursor()
            oid = request.form['id']
            cur.execute('select payment, black, brown from mulch.orders where id = %s', (oid,))
            stat = cur.fetchone()
            cur.execute('update mulch.orders set payment = not payment where id = %s', (oid,))
            if stat[0] is False:
                cur.execute('update mulch.logins set bags_verified = bags_verified + %s where username = %s', (
                    stat[1] + stat[2],
                    user,
                ))
            else:
                cur.execute('update mulch.logins set bags_verified = bags_verified - %s where username = %s', (
                    stat[1] + stat[2],
                    user,
                ))
        alert = '{}'.format('paid' if stat[0] is True else 'not paid')
    with con:
        cur = con.cursor()
        cur.execute('select * from mulch.orders where seller = %s order by payment ASC, name ASC', (user,))
        orders = cur.fetchall()
        cur.execute('select * from mulch.logins where username = %s', (user,))
        userinfo = cur.fetchone()
    for o in orders:
        bags += (o[6] + o[7])
        phones[o[0]] = format_phone(o[5])
    if not orders:
        return render_template('sales.html', err='There are no orders tied to this account.')
    return render_template('sales.html', count=userinfo[3], orders=orders, bags=userinfo[4], verified=userinfo[5],
                           phones=phones, admin='yes', fullname=fullname, alert=alert)


@app.route('/order/<order>', methods=['GET', 'POST'])
@app.route('/orders/<order>', methods=['GET', 'POST'])
@requires_admin
def order_edit(order):
    if request.method == 'GET':
        with con:
            cur = con.cursor()
            cur.execute('select * from mulch.orders where id = %s', (order,))
            orderinfo = cur.fetchone()
        return render_template('orderedit.html', order=orderinfo)

    name = request.form['name']
    email = request.form['email']
    phone = request.form['phone']
    phone = phonenumbers.format_number(
        phonenumbers.parse(phone, 'US'),
        phonenumbers.PhoneNumberFormat.E164)[2:]
    address = request.form['address']
    zipcode = request.form['zip']
    brown = int(request.form['brown'])
    black = int(request.form['black'])
    orderid = request.form['orderid']
    paid = True if 'paid' in request.form else False
    instructions = session['instructions'] = request.form['instructions']
    ref = request.form['ref']
    if black + brown < 5:
        err = 'You must order at least five bags of mulch.'
        with con:
            cur = con.cursor()
            cur.execute('select * from mulch.orders where id = %s', (order,))
            orderinfo = cur.fetchone()
        return render_template('orderedit.html', order=orderinfo, error=err)
    with con:
        cur = con.cursor()
        cur.execute(
            'update mulch.orders set name = %s, email = %s, address = %s, zip = %s, phone = %s, brown = %s, black = %s, '
            'seller = %s, instructions = %s, payment = %s where id = %s', (
                name, email, address, zipcode, phone, brown, black, ref, instructions, paid, orderid,))
    with con:
        cur = con.cursor()
        cur.execute('select sum(COALESCE(brown,0) + COALESCE(black,0)) from mulch.orders where seller = %s', (ref,))
        data = cur.fetchone()
        cur.execute(
            'select sum(COALESCE(brown,0) + COALESCE(black,0)) from mulch.orders where seller = %s and payment = TRUE',
            (ref,))
        data2 = cur.fetchone() or [0]

        cur.execute('update mulch.logins set bags = %s, bags_verified = %s where username = %s', (
            data[0], data2[0], ref,))

    if 'errortype' in session:
        del session['errortype']
    session['orderid'] = orderid
    session['success'] = True
    return redirect(f'/sales/{ref}')


@app.route('/leaderboard', methods=['GET', 'POST'])
@requires_auth
def leaderboard():
    if request.method == 'POST':
        with con:
            cur = con.cursor()
            cur.execute('select username from mulch.logins where site = %s', (g.site,))
            users = cur.fetchall()
            for u in users:
                cur.execute(
                    'select sum(COALESCE(brown,0) + COALESCE(black,0)) from mulch.orders where seller = %s and site = %s',
                    (u, g.site,))
                data = cur.fetchone()
                cur.execute(
                    'select sum(COALESCE(brown,0) + COALESCE(black,0)) from mulch.orders where seller = %s and payment = TRUE and site = %s',
                    (u, g.site,))
                data2 = cur.fetchone() or [0]

                cur.execute('update mulch.logins set bags = %s, bags_verified = %s where username = %s and site = %s', (
                    data[0] or 0, data2[0] or 0, u, g.site,))
    nums = {}
    i = 1
    with con:
        cur = con.cursor()
        cur.execute('select * from mulch.logins where site = %s order by bags_verified desc, orders desc', (g.site,))
        data = cur.fetchall()
    for d in data:
        nums[d[0]] = i
        i += 1
    return render_template('leaderboard.html', data=data, nums=nums)


@app.errorhandler(403)
def forbidden(error):
    return render_template('403.html'), 403


@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


@app.route('/stripe-wh', methods=['POST'])
def webhook():
    eventjson = request.get_json()
    pprint(eventjson)
    eventdata = eventjson['data']['object']
    print(eventjson['type'])
    customer = stripe.Customer.retrieve(eventdata['customer'])
    if eventjson['type'] == 'charge.succeeded':
        with con:
            cur = con.cursor()
            cur.execute('insert into mulch.orders values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)', (
                eventdata['payment_intent'],
                customer['shipping']['name'],
                customer['email'],
                customer['shipping']['address']['line1'],
                customer['shipping']['address']['postal_code'],
                customer['shipping']['phone'],
                eventdata['description'].split(' ')[0],
                eventdata['description'].split(' ')[5],
                eventdata['metadata']['referrer'],
                eventdata['metadata']['instructions'] if 'instructions' in eventdata['metadata'] else None,
                True,
                g.site,
            ))
            cur.execute(
                'update mulch.logins set orders = orders + 1, bags = bags + %s, bags_verified = bags_verified + %s '
                'where username = %s', (
                    int(eventdata['description'].split(' ')[0]) + int(eventdata['description'].split(' ')[5]),
                    int(eventdata['description'].split(' ')[0]) + int(eventdata['description'].split(' ')[5]),
                    eventdata['metadata']['referrer'],
                ))
    return flask.make_response(flask.jsonify({'success': 'true'}), 200)


@app.route('/api/payment_status', methods=['POST'])
@requires_admin
def payment_status():
    with con:
        cur = con.cursor()
        oid = request.form['id']
        user = request.form['user']
        cur.execute('select payment, black, brown from mulch.orders where id = %s', (oid,))
        stat = cur.fetchone()
        cur.execute('update mulch.orders set payment = not payment where id = %s', (oid,))
        if stat[0] is False:
            cur.execute('update mulch.logins set bags_verified = bags_verified + %s where username = %s', (
                stat[1] + stat[2],
                user,
            ))
        else:
            cur.execute('update mulch.logins set bags_verified = bags_verified - %s where username = %s', (
                stat[1] + stat[2],
                user,
            ))
    return jsonify({'order': oid, 'new_status': not stat[0]})


@app.route('/dashboard')
def dash():
    return render_template('dashboard.html')


@app.route('/api/stats', methods=['GET'])
def sales_statistics():
    site = g.site
    with con:
        cur = con.cursor()
        data = []
        for x in range(4):
            cur.execute(
                f"select count(*) from mulch.orders where "
                f"created < date_trunc('week', current_date - interval '{x - 1} week') and "
                f"created > date_trunc('week', current_date - interval '{x} week') and site = %s",
                (site,))
            data.append(cur.fetchone()[0])
    return jsonify({'counts': data})


if __name__ == '__main__':
    app.run(debug=True, port=65010)
