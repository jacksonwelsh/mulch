from sqlalchemy.ext.declarative import declarative_base
import sentry_sdk
import inspect
from sentry_sdk.integrations.flask import FlaskIntegration
from flask import Flask, Blueprint
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from .usertools import *

sentry_sdk.init(
    dsn="https://8a7b9412b4684d84985c156bdccdaf81@sentry.io/1440786",
    integrations=[FlaskIntegration()],
    release='mulch2'
)

Base = declarative_base()
db = SQLAlchemy()
login_manager = LoginManager()

app = Flask(__name__)

app.url_map.strict_slashes = False
app.jinja_env.cache = {}
app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True
app.jinja_env.auto_reload = True
app.config['SECRET_KEY'] = 'öhuoiegrrr98yiF*^(fO*G#Ubejkhiyf973g8owuyiF(&OGUE#IF(&*DOUEIVF379G8HWDBW*E(GF&uogi9feg80(&'

app.config['SQLALCHEMY_DATABASE_URI'] = f'postgresql://ubuntu:{get_secret("sql", "password")}@localhost/band'

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


from . import blueprints
from .models import *

db.init_app(app)
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    # since the user_id is just the primary key of our user table, use it in the query for the user
    return User.query.get(int(user_id))


isBlueprint = lambda blueprint: (isinstance(blueprint, Blueprint))
blueprints = [blueprint[1] for blueprint in inspect.getmembers(blueprints, isBlueprint)]
for blueprint in blueprints:
    print(blueprint.name)
    app.register_blueprint(blueprint)
