from werkzeug.security import generate_password_hash, \
    check_password_hash
import psycopg2
import phonenumbers
import os
import json
from .models import *
from sqlalchemy import func
from . import db


def valid_user(user=None, name=None, site='global'):
    if user:
        if User.query.filter_by(username=user.lower(), site=site).first():
            return True
        return False
    elif name:
        if User.query.filter(func.lower(User.name) == name.lower(), User.site == site).first():
            return True
        return False


def validate_login(user, password, site):
    if valid_user(user=user, site=site):
        if check_password(user, password, site):
            if check_admin(user, site) is True:
                return True
            return False
    return None


def create_user(name, username, password, site):
    if not valid_user(user=username):
        new_user = User(
            username=username.lower(),
            password=generate_password_hash(password),
            name=name,
            admin=False,
            site=site
        )
        db.session.add(new_user)
        db.session.commit()
        return True
    return None


def delete_user(user, site):
    user = User.query.filter_by(username=user, site=site).first()
    db.session.delete(user)
    db.session.commit()


def set_password(user, password, site):
    pw_hash = generate_password_hash(password)
    if valid_user(user=user, site=site):
        user = User.query.filter_by(username=user.lower(), site=site).first()
        user.password = pw_hash
        db.session.add(user)
        db.session.commit()
        return True
    return None


def check_password(user, password, site):
    usr = User.query.filter_by(username=user.lower(), site=site).first()
    if usr:
        return check_password_hash(usr.password, password)
    return False


def check_admin(user, site):
    usr = User.query.filter_by(username=user.lower(), site=site).first()
    if usr:
        return usr.admin
    return None


def get_name(user, site):
    usr = User.query.filter_by(username=user.lower(), site=site).first()
    if usr:
        return usr.name
    return None


def get_username(name, site):
    user = User.query.filter(func.lower(User.name) == name.lower(), User.site == site).first()
    if user:
        return user.username
    return None


def format_phone(number):
    return phonenumbers.format_number(
        phonenumbers.parse(number, 'US'),
        phonenumbers.PhoneNumberFormat.NATIONAL)


def get_secret(service, token='null'):
    secrets_path = os.path.join(os.path.abspath(os.path.dirname(__file__)))
    with open("{}/secrets.json".format(secrets_path)) as data:
        s = json.load(data)
        if token == 'null':
            secret = s['{}'.format(service)]
        else:
            secret = s['{}'.format(service)]['{}'.format(token)]
        # logger.debug("EXIT secrets: {}".format(len(secret)))
    return secret


con = psycopg2.connect(host="localhost",
                       database="band",
                       user="ubuntu",
                       password=get_secret('sql', 'password'),
                       port=5432)
