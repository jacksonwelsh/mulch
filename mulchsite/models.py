from sqlalchemy import Column, ForeignKey, Integer, String, Float, Boolean, Index, Text, BigInteger
from sqlalchemy.dialects.postgresql import TIMESTAMP as Timestamp
from sqlalchemy.orm import relationship, backref
from . import db, Base

current_environment = 'mulch_test'


class Site(db.Model):
    __tablename__ = 'sites'
    __table_args__ = {'schema': current_environment}
    slug = Column(Text, nullable=False, unique=True, primary_key=True)
    fullname = Column(Text, nullable=False)
    license = Column(Text)
    nav_img = Column(Text)
    header_img = Column(Text)
    tagline = Column(Text)
    color = Column(Text)
    domain = Column(Text)
    nav_text = Column(Text)
    header_text = Column(Text)


class User(db.Model):
    __tablename__ = 'logins'
    __table_args__ = {'schema': current_environment}
    id = Column(Integer, nullable=False, unique=True, primary_key=True, autoincrement=True)
    username = Column(Text, nullable=False)
    password = Column(Text, nullable=False)
    name = Column(Text, nullable=False)
    orders = Column(Integer, nullable=False, server_default=db.text('0'))
    bags = Column(Integer, nullable=False, server_default=db.text('0'))
    bags_verified = Column(Integer, nullable=False, server_default=db.text('0'))
    admin = Column(Boolean, nullable=False, server_default=db.text('false'))
    site = Column(Text, ForeignKey(Site.slug), nullable=False)


class Order(db.Model):
    __tablename__ = 'orders'
    __table_args__ = {'schema': current_environment}
    id = Column(Text, nullable=False, unique=True, primary_key=True)
    name = Column(Text)
    email = Column(Text)
    address = Column(Text)
    zip = Column(Text)
    phone = Column(Text)
    brown = Column(Integer)
    black = Column(Integer)
    seller = Column(Text)
    instructions = Column(Text)
    payment = Column(Boolean)
    site = Column(Text, ForeignKey(Site.slug), nullable=False)
    created = Column(Timestamp)


class Product(db.Model):
    __tablename__ = 'products'
    __table_args__ = {'schema': current_environment}
    id = Column(Integer, nullable=False, unique=True, primary_key=True, autoincrement=True)
    name = Column(Text, nullable=False)
    description = Column(Text)
    price = Column(Integer)
    site = Column(Text, ForeignKey(Site.slug), nullable=False)
    image = Column(Text, nullable=False)


class Customer(db.Model):
    __tablename__ = 'users'
    __table_args__ = {'schema': current_environment}
    id = Column(Text, unique=True, primary_key=True, nullable=False)
    name = Column(Text)
    phone = Column(Text)
    email = Column(Text)
    site = Column(Text, ForeignKey(Site.slug), nullable=False)
